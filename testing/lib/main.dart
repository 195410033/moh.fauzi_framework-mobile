import 'package:flutter/material.dart';
import 'package:sp_util/sp_util.dart';
import 'package:testing/screens/homepage.dart';
import 'package:testing/screens/register.dart';
import 'package:testing/screens/login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SpUtil.getInstance();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        'login-page': (context) => Login(),
        'registrasi-page': (context) => register(),
        'home-page': (context) => HomePage(),
      },
      initialRoute: 'login-page',
    );
  }
}
