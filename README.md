# UAS Framework Mobile
NIM : 195410033

Nama : Moh.Fauzi

# Back-end
- Jalankan perintah berikut ini untuk menjalankan server lokal `API` pada direktori `BelajarAPI`:
```
php artisan serve
```

- Kemudian aktifkan `Apache` dan `MySQL` pada XAMPP, dan jalankan di browser localhost `PhpMyAdmin`

- Kemudian buat database baru dengan nama `passport`, dan import file `passport.sql`.

- Kemudian pada Visual Studio Code, folder `testing` akan melakukan run & debug untuk menginstall projek tersebut di simulator android yang dijalankan.