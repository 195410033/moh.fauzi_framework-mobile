-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2022 at 06:53 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `passport`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('029beeaf07f792f72b77ddd88dbe7863bc3f10a51828fa1cf3c8eb97aa7b5f39270b291f35340410', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:31', '2022-06-26 17:20:31', '2023-06-27 00:20:31'),
('05533c75653c532fe6f829669c7784a72a727fec8c9d8b6666102b3f9229b75bbd1795847ce8bbdb', 7, 1, 'user', '[]', 0, '2022-06-26 18:42:36', '2022-06-26 18:42:36', '2023-06-27 01:42:36'),
('0660ffcc7ec85e8881d4150a1dfa01df458b9a38cba01972ffdca809dc7776b53b66fc7dde9c5652', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 16:58:55', '2022-06-26 16:58:55', '2023-06-26 23:58:55'),
('09ae48960eb83016ee64d7f88b79e3af40905c1426cca7a08a2dec8909c23fbc9492419ba3401a0d', 7, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:35:19', '2022-06-26 17:35:19', '2023-06-27 00:35:19'),
('0cb7dc5e7e370ec84567c3bc4ee614962624d045d61487ecfa5c719bbd9d22a2eb23a9ad0aa85628', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:33', '2022-06-26 17:20:33', '2023-06-27 00:20:33'),
('13bdafdf81507815fd25c72429e3aba2798e17dcc1205669fdf5d9ee7afb7b32c01a83af86cf358f', 2, 1, 'user', '[]', 0, '2022-06-26 17:40:07', '2022-06-26 17:40:07', '2023-06-27 00:40:07'),
('1532afcf23f4ef6499b90511796bfa1744d73a39d80d18b4cb16379390fa16c971830c7bfbc8505f', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:23:48', '2022-06-26 17:23:48', '2023-06-27 00:23:48'),
('18ac984b8d67f93d3d00cc16c8dac1cfcbc113f03fa8c582f515a4df2f1ca0b9bb8afa615b14dddf', 2, 1, 'user', '[]', 0, '2022-06-26 17:33:01', '2022-06-26 17:33:01', '2023-06-27 00:33:01'),
('1991f4f773e631f642c4fb140d6632d294d6a2b56a7847af26078bb7534ebbd89697735b8363e8e7', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:32', '2022-06-26 17:20:32', '2023-06-27 00:20:32'),
('1d4547742d66d57c866f5c6369f115cf551fcec0b5b0e5c2c7396c6271cffc2d7166eea1bba0c3b1', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:21', '2022-06-26 17:20:21', '2023-06-27 00:20:21'),
('1e447401d3a38d1ee50c77f4d1d84ccf91370b762568579802781838949d76b27b53cbe43add5c5d', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 15:37:58', '2022-06-26 15:37:58', '2023-06-26 22:37:58'),
('23e54214ededf7cdbc3718a761ac00aeffeba64075cc67d74d46991a115db0ee9a76be36963ce616', 2, 1, 'user', '[]', 0, '2022-06-26 21:01:40', '2022-06-26 21:01:40', '2023-06-27 04:01:40'),
('26b425f7770de75cc5ddc5beee91b6d58588ba9858da11beaf7f62bdbc869517bd48024ef43ebc90', 7, 1, 'user', '[]', 0, '2022-06-26 21:01:14', '2022-06-26 21:01:14', '2023-06-27 04:01:14'),
('2912d73d66515e048f7d89ee4cb43eb8fa2576b1236cf0053385bdcd48f0b87fe26ce9323add924f', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 16:57:34', '2022-06-26 16:57:34', '2023-06-26 23:57:34'),
('30d8f3294b696853ade0d4a111e0a8a9beee1dbbd047f22d114c28424960b043f7c692ec7f1561ba', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:17', '2022-06-26 17:20:17', '2023-06-27 00:20:17'),
('3248f6cb74397a0c8460172a9b6fcfc72a64b8d0cb9719c2c76c20d60b9f47bb25b07d959092b41c', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:28', '2022-06-26 17:20:28', '2023-06-27 00:20:28'),
('337f6290a54adbf3b4b63def10f2731969ee3ab6dade372fc6c3dbb87f911ec36942ff2f3031a8c4', 4, 1, 'LawanKovid', '[]', 0, '2022-06-26 13:22:56', '2022-06-26 13:22:56', '2023-06-26 20:22:56'),
('3ae7707d64fed72f0e8f5f135bfd6bee3a82d07865a4979bd32c7a9b2400dbe0c786c14cd035c172', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:09:40', '2022-06-26 17:09:40', '2023-06-27 00:09:40'),
('3d4fa7ba9c09b029835133126bc69bba020a7de1d4935c4b35a7e851dbe8c99db9eaf52d6a442ad6', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:31', '2022-06-26 17:20:31', '2023-06-27 00:20:31'),
('402b4b010e3f6d7cf24331169b42765cd3d7ef66d82164b525f7fff579c49b8f233d58900a1ffdf4', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:10:52', '2022-06-26 17:10:52', '2023-06-27 00:10:52'),
('43dee072ebba4bfd88720ba756394897ea0cec48a1d552b21da883bc20a37203c1d026e7cc19f0ab', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:09:52', '2022-06-26 17:09:52', '2023-06-27 00:09:52'),
('4be3c74be330a9da244592f0dc30a0b437be4c397cd88e5b315137e73158079bc1d3966994034e3c', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 13:45:22', '2022-06-26 13:45:22', '2023-06-26 20:45:22'),
('4f80c1bda5fdd39fcc282e2d2cb1546fc44919208df69c0df2c563c3dd6f9bd587d9bc191952e1d8', 2, 1, 'user', '[]', 0, '2022-06-27 06:22:50', '2022-06-27 06:22:50', '2023-06-27 13:22:50'),
('52d618e9f157f0532508e2cf4b9c1e70b207847a723a791ec0ebc2be2d0d1c8cdcd9d73dae05ff83', 7, 1, 'user', '[]', 0, '2022-06-26 19:49:24', '2022-06-26 19:49:24', '2023-06-27 02:49:24'),
('5e98754144ce2794da63f815e3ca6c19bb4ddae12d7f4efa40d226dad6b836f137234a905318f431', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:33', '2022-06-26 17:20:33', '2023-06-27 00:20:33'),
('65a6eff3c70699c383f3584aec1d697eb9538f5abb83012c8a9736f93ad650f5e78db0e083ce315e', 6, 1, 'LawanKovid', '[]', 0, '2022-06-26 15:52:47', '2022-06-26 15:52:47', '2023-06-26 22:52:47'),
('6b9ddc7b975827ca3407987a9de1e7f9f144c04be245fec9b5ea2b38058baaed349b19935892a156', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 15:29:32', '2022-06-26 15:29:32', '2023-06-26 22:29:32'),
('79c1df7e1569cfa546bfe82f2a41dcca7912d76bddcc76382e76d88eada268d9a2df882d879efe55', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:30', '2022-06-26 17:20:30', '2023-06-27 00:20:30'),
('7dd11fa5370ca1e5a7d0c4101a8ddfc5b308d49ee506c8651117bfd60d3519b50ca9fd60e9cc3de0', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:15:56', '2022-06-26 17:15:56', '2023-06-27 00:15:56'),
('7f311e7fc01b2d5c92b40cc7db992a22dc7e6745be6cf155e02c9d80e6acb973147e2789a51ff619', 4, 1, 'LawanKovid', '[]', 0, '2022-06-26 13:31:40', '2022-06-26 13:31:40', '2023-06-26 20:31:40'),
('89c94d78af637c4dadf9e2ba4d78f5e987214c815d900fc11dc9fe27dfe6473aa7a0644142c2be25', 7, 1, 'user', '[]', 0, '2022-06-26 18:45:38', '2022-06-26 18:45:38', '2023-06-27 01:45:38'),
('8c51cecd13a9bcf149a89d84ac04067fe362f1b3eb1938ffc2e577d5900470ab60a1bb65c55ff73d', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:13:26', '2022-06-26 17:13:26', '2023-06-27 00:13:26'),
('8f7497051fb91d8ba074d8f769774858020e045794343bf7d9cbf71a7d4de30723d4864bb65eb36d', 8, 1, 'LawanKovid', '[]', 0, '2022-06-27 06:24:58', '2022-06-27 06:24:58', '2023-06-27 13:24:58'),
('9488426e2e933b6c1614f2ad9f4ce904b656e3a4cab01ea6e75881038597104f868a89ba8d5f5db2', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:10:37', '2022-06-26 17:10:37', '2023-06-27 00:10:37'),
('9a5db5982a9f416ebefc3fbc6f23598390920fe6a2a7c95ae5f389eafaa2a84d4e8dc19da7692e7a', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 16:56:47', '2022-06-26 16:56:47', '2023-06-26 23:56:47'),
('9b03b6aadf6f2bc70bf8524525e26ccec86e15ab772d81930e359d5cf3403a0b02f33d93a3b92ae3', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:21:05', '2022-06-26 17:21:05', '2023-06-27 00:21:05'),
('a889f24d6df9e76d28a0a16cc70d7a9d824002640d8c8ea8d3110c58120b6cd351d82797ae610db1', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:30', '2022-06-26 17:20:30', '2023-06-27 00:20:30'),
('a9703c60a9308b15f5aa63fdb376c89b0b8818f1e1c0264226a5426960ffc95105f3dea903a8350d', 7, 1, 'user', '[]', 0, '2022-06-26 19:48:04', '2022-06-26 19:48:04', '2023-06-27 02:48:04'),
('ac54dc12f18b67d29268ce581360ae994abb9e6856538f20ea59d4619821ddc96c1cf9abbe09c1b1', 3, 1, 'LawanKovid', '[]', 0, '2022-06-26 09:30:16', '2022-06-26 09:30:16', '2023-06-26 16:30:16'),
('b06beb00e46cd50816e02540bc0191eeb32ec42d2b49f7e38a32d3f2acbd1258521500e32909b087', 2, 1, 'user', '[]', 0, '2022-06-27 06:23:42', '2022-06-27 06:23:42', '2023-06-27 13:23:42'),
('b5b4a8bd67cc3d1b2fe8d1fe71a0120841f627295417dd19e8383c77b651ec8a7fab4af5588656bb', 7, 1, 'user', '[]', 0, '2022-06-26 20:33:58', '2022-06-26 20:33:58', '2023-06-27 03:33:58'),
('b7f8196f76f4033ff221f3e5d099534faa73cf5a9cb990457d2b97de71e69c4c0118835114b8a92c', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 16:46:11', '2022-06-26 16:46:11', '2023-06-26 23:46:11'),
('b8e929cf78fe2474d50ae7c0cdc1b1aa6f4f92bda5573cf3c2f2574ff1a0a70d815122b0fd21196b', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 16:52:47', '2022-06-26 16:52:47', '2023-06-26 23:52:47'),
('bbb12af6993eb2ffb7f99879ff421eec290f4f2a4b0cd82d99182ef8373cbec1d336fdde8434cd3e', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:12', '2022-06-26 17:20:12', '2023-06-27 00:20:12'),
('bdb81db497eb29dade114fec4906fb5408859aedf30ea892d5a0207526d1ec652b49ce3719bb59e2', 2, 1, 'user', '[]', 0, '2022-06-26 17:32:05', '2022-06-26 17:32:05', '2023-06-27 00:32:05'),
('c3bca41494bb9b7c9f91cf2a71bf54ec1aba0324c8dc03761e9d1ea5a60fc57aefc22a26e10f4f15', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:10:17', '2022-06-26 17:10:17', '2023-06-27 00:10:17'),
('c59594573136f6230feaf7c7889607d7361b8907f039cc44fa3766c2aa0d3d0ab9393e39be4fd113', 7, 1, 'user', '[]', 0, '2022-06-26 18:25:26', '2022-06-26 18:25:26', '2023-06-27 01:25:26'),
('cd48f438e52d39780fb64916f2595bbbe4f2d4662747a0f3d101723ca74b0d61aaa4b1f6f2abe618', 5, 1, 'LawanKovid', '[]', 0, '2022-06-26 13:23:53', '2022-06-26 13:23:53', '2023-06-26 20:23:53'),
('cda252665e532313d2300efb6d001323f6af656beab66da07e6622d6acc147bc13642b7ff59185d6', 2, 1, 'user', '[]', 0, '2022-06-26 18:51:50', '2022-06-26 18:51:50', '2023-06-27 01:51:50'),
('ce4ef2e152e53c96bfb597b43bdb583f9ac477991c67780ca9bcc443d2bfd1b85e531d1a528d672b', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 15:33:57', '2022-06-26 15:33:57', '2023-06-26 22:33:57'),
('d2fb4edcb6f1a1386a8264d02b8911fb811a0f4697717df089540b88444bd911f05332afc6e825f8', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 16:58:39', '2022-06-26 16:58:39', '2023-06-26 23:58:39'),
('d374dcc5d50b03a93f005c030c49569072a25e29997c7235b8a1c2a2d755cb517a84207ea84bbcfc', 6, 1, 'LawanKovid', '[]', 0, '2022-06-26 15:53:10', '2022-06-26 15:53:10', '2023-06-26 22:53:10'),
('df0de205cb16773c9af5a3d517dc6bd52f901b9a3e8e7e78e3914ef217b5bebb698573093f3f6b89', 8, 1, 'user', '[]', 0, '2022-06-27 06:25:44', '2022-06-27 06:25:44', '2023-06-27 13:25:44'),
('e2e437ae1f5e6c3866b3707866492885cb05bd26b7cbce70f0643198f6651e294ea30e050701aff3', 3, 1, 'LawanKovid', '[]', 0, '2022-06-26 09:29:47', '2022-06-26 09:29:47', '2023-06-26 16:29:47'),
('e4292ffa10449d866354770d9fd067505e01e0b2a0de458f68ef2cde16b6b365224124d9c72e0510', 7, 1, 'user', '[]', 0, '2022-06-26 21:11:57', '2022-06-26 21:11:57', '2023-06-27 04:11:57'),
('e4938fda48c15a30eb35f3764efb3448fa96d130d0c0d57931496347493833a30819aa0fda00ed19', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:33', '2022-06-26 17:20:33', '2023-06-27 00:20:33'),
('e9556e51a71295ec9fbd58e74b203ba147189b3ed8856c67a6a310e9077e10849772012865e0bbcc', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:12', '2022-06-26 17:20:12', '2023-06-27 00:20:12'),
('e9b23839cc93fa300ab81993cbcbc4daef7da163de08940d077ce60040d082ce61ffc9ff9b8b3ef4', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:20:32', '2022-06-26 17:20:32', '2023-06-27 00:20:32'),
('f1e1c9053929d6d1037066874061c9a57f0aa2d6c67d8c66811c82f05dff64aaa9df0485c648bf6d', 7, 1, 'user', '[]', 0, '2022-06-27 06:19:33', '2022-06-27 06:19:33', '2023-06-27 13:19:33'),
('f36b35e7a72dc4dbbc3a447d6bffa901effb7bbbca785b2fd72afed413a9ea4ffa3f3ac223bf7706', 7, 1, 'user', '[]', 0, '2022-06-26 19:44:16', '2022-06-26 19:44:16', '2023-06-27 02:44:16'),
('f3f5b98fc8bfd86fcb3ae8abe3cf7e52dc31d9716ce8d483ad4021fde54203fd18de73836f28f20a', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 14:56:03', '2022-06-26 14:56:03', '2023-06-26 21:56:03'),
('fae358a39067468262cdb31f2b377e21b66d2c3f216a6ee4890a59b5917398fb8f1023c4f949c6ad', 2, 1, 'LawanKovid', '[]', 0, '2022-06-26 17:21:18', '2022-06-26 17:21:18', '2023-06-27 00:21:18'),
('ffb3ec72972f2155eb8dc741a56d33b601b3ba047968f51133bed66651f9c1117f4f337a47a01ab2', 7, 1, 'user', '[]', 0, '2022-06-26 20:50:22', '2022-06-26 20:50:22', '2023-06-27 03:50:22');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'WcTKMjVK4i2MsKZFl1nbossZOfinVc9YFRtUoXZH', NULL, 'http://localhost', 1, 0, 0, '2022-06-26 09:29:31', '2022-06-26 09:29:31'),
(2, NULL, 'Laravel Password Grant Client', '1q9aslODB5VJ7jrHhIDLchSObSN6V9LbfRfyZuip', 'users', 'http://localhost', 0, 1, 0, '2022-06-26 09:29:31', '2022-06-26 09:29:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-06-26 09:29:31', '2022-06-26 09:29:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'moh.fauzi', 'fauzi@gmail.com', NULL, '$2y$10$RVr94mV7Okc5tUR/GEbz3O0PGQYt0MpDoDwHhvCR3lTIK4XO01cdK', NULL, '2022-06-26 09:16:06', '2022-06-26 09:16:06'),
(6, 'jin', 'jin@gmail.com', NULL, '$2y$10$Svey4BQZf6a8DsC3MNrwXevKZw2UHDyXFX.dcN9KlDroMsRpsx5Km', NULL, '2022-06-26 15:52:47', '2022-06-26 15:52:47'),
(7, 'admin', 'admin@admin.com', NULL, '$2y$10$ieoU7lzxLFxDGYPVbNvfEO/jPnUtM8amom6A5LYQUwMIbF4DJHreu', NULL, '2022-06-26 17:35:19', '2022-06-26 17:35:19'),
(8, 'taufik', 'taufik@gmail.com', NULL, '$2y$10$BFpPkIIxrokK50drzCTZM.zbuKlUOzMnojpy7WQNQt/IE68R2ZlGa', NULL, '2022-06-27 06:24:58', '2022-06-27 06:24:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
